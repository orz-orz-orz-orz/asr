import os
import pathlib
import torch


class Tokenizer:
    def __init__(self, tok_list: os.PathLike):
        tok2idx = {}
        idx2tok = []
        i = 0
        for line in pathlib.Path(tok_list).open("r").readlines():
            if line.isspace() or line.startswith('#'):
                continue
            tok2idx[line.rstrip()] = i
            idx2tok.append(line.rstrip())
            i += 1
           
        self.tok2idx = tok2idx
        self.idx2tok = idx2tok
        
    def encode(self, s, insert_br=False):
        # parse the s
        # 注意 "ガ".isalpha() == True; "中".isalpha() == True; 
        tokens = []
        state = 0 # 0: initial state, 1: chinese, 2: japanese, 3: tag
        i = 0
    
        buf = ""
        while i <= len(s):
            c = s[i] if i < len(s) else "\x00" # append "<EOS>"
            if state == 0:
            # check current letter
                if c.isspace() or c == '\x00': # ignore space
                    pass
                elif c.isascii() and c.isalpha():
                    state = 1
                    buf = c
                elif ord(c) >= 12449 and ord(c) <= 12540:
                    if c in ['ャ', 'ュ', 'ョ', 'ァ', 'ィ', 'ゥ', 'ェ', 'ォ']:
                        raise ValueError("token 開頭不能是小文字")
                    elif c in ['・', 'ヵ', 'ヶ']:
                        raise ValueError("token 不能含有・, ヵ, ヶ")
                    else:
                        state = 2
                        buf = c
                elif c == "<":
                    state = 3
                    buf = c
                else:
                    raise ValueError(f"碰到未知的字元 {c}")
            elif state == 1:
                if c.islower():
                    buf += c
                elif c.isnumeric():
                    state = 0
                    buf += c
                    tokens.append(buf)
                    buf = ""
                else:
                    raise ValueError(f"{buf} 拼音不完整(發音+聲調)")
            elif state == 2:
                # check current letter
                if ord(c) >= 12449 and ord(c) <= 12540:
                    if c in ['ャ', 'ュ', 'ョ', 'ァ', 'ィ', 'ゥ', 'ェ', 'ォ']:
                        # 小文字用附加的
                        buf += c 
                    elif c in ['・', 'ヵ', 'ヶ']:
                        raise ValueError("token 不能含有・, ヵ, ヶ")
                    else:
                        # 是其他大文字，清空 buf
                        tokens.append(buf)
                        buf = c
                elif c.isspace() or c == '\x00': # 是空白
                    state = 0
                    tokens.append(buf)
                    buf = ""
                elif c.isascii() and c.isalpha(): # 是中文
                    state = 1
                    tokens.append(buf)
                    buf = c
                else:
                    raise ValueError(f"碰到未知的字元 {c}")
            elif state == 3:
                if c.isspace():
                    raise ValueError("tag 裡面不能有空白")
                elif c == '<':
                    raise ValueError("tag 裡面不能有<")
                elif c == '>':
                    state = 0
                    buf += c
                    tokens.append(buf)
                    buf = ""
                elif c.isalpha() or c == '_':
                    buf += c                   
                else:
                    raise ValueError(f"碰到未知的字元 {c}")
                
            i += 1
        if state != 0 or buf != "":
            raise ValueError("不完整的序列？")
            
        
        if insert_br:
            new_tokens = []
            for t in tokens:
                if t != "ッ" and t != "ー" and t != "<br>":
                    new_tokens.append("<br>")
                new_tokens.append(t)
            new_tokens.append("<br>")
            tokens = new_tokens
        
        tokens.append("<eos>")
        
        indices = torch.tensor([self.tok2idx[t] for t in tokens])
        
        return indices
    
    def decode(self, indices): 
        return "".join([self.idx2tok[i] for i in indices])