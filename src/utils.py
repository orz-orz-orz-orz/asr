class JSDict(dict):
    """
    The Java Script-like Dictionary
    """
    def __init__(self, dict_):
        super().__init__(dict_)
        for key in self:
            item = self[key]
            if isinstance(item, list):
                for idx, it in enumerate(item):
                    if isinstance(it, dict):
                        item[idx] = JSDict(it)
            elif isinstance(item, dict):
                self[key] = JSDict(item)

    def __getattr__(self, key):
        try:
            return self[key]
        except KeyError:
            raise AttributeError(key)

    def __setattr__(self, key, item):
        if isinstance(item, dict):
            item = JSDict(item)
        if isinstance(item, list):
            for idx, it in enumerate(item):
                if isinstance(it, dict):
                    item[idx] = JSDict(it)
        self[key] = item

    def __getstate__(self):
        # avoid KeyError: '__getstate__'
        return self.__dict__


def reversed_enumerate(list_):
   for index in reversed(range(len(list_))):
      yield index, list_[index]