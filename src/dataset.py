import os
import pathlib
import torchaudio.sox_effects
from torchaudio.transforms import MelSpectrogram
from torch.utils.data import Dataset
from .tokenizer import Tokenizer

class MspTokDataset(Dataset):
    def __init__(self, data_path: os.PathLike, scp_path: os.PathLike,
                 tokenizer: Tokenizer, melspec: MelSpectrogram):
        super().__init__()
        self.data = pathlib.Path(data_path)
        scp =  []
        for line in pathlib.Path(scp_path).open("r").readlines():
            prefix, filename = line.split()
            scp.append((prefix, filename))  
        self.scp = scp
        self.sox_effects = [
            ['remix', '-'],
            ['rate', f'{melspec.sample_rate}']
        ]
        self.tokenizer = tokenizer
        self.melspec = melspec
        
    def __len__(self):
        return len(self.scp)
    
    def __getitem__(self, idx):
        prefix, filename = self.scp[idx]
        wav_path = self.data / prefix / 'wav' / f'{filename}.wav'
        pron_path = self.data / prefix / 'pron' / f'{filename}.txt'
        # read the wav
        wav, _ = torchaudio.sox_effects.apply_effects_file(
            wav_path, self.sox_effects, channels_first=True
        )
        wav = wav.view(-1)
        melspec = self.melspec(wav).transpose(0, 1) # (T, K)
        
        # read the pron
        pron = pron_path.read_text()
        indices = self.tokenizer.encode(pron, insert_br=False)
        return prefix, filename, melspec, indices
    