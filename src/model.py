import torch
import torch.nn as nn
import torch.nn.functional as F


class LSTMP(nn.Module):
    def __init__(self, in_channels, num_classes, h_channels, num_layers=1, dropout=0.01):
        super().__init__()
        self.embeddings = nn.Parameter(torch.randn(num_classes, h_channels))
        self.lstm = nn.LSTM(in_channels, h_channels, num_layers, dropout=dropout, batch_first=False)
        self.dropout = dropout

    def forward(self, x, hx=None, y=None, hy=None):
        # x: (B, T, C), input features
        # hx: ((L, B, H), (L, B, H)) previous hidden states
        # y: (B, T), indices current answer
        # hy: (B), index of previous answer
        """
        p_list = []
        if hy is None:
            hy = torch.ones(1, x.shape[0], device=x.device, dtype=torch.long)
        e = F.embedding(hy, self.embeddings) # (1, B, H)
        for t in range(x.shape[1]):
            h = x[None, :, t, :] #(1, B, C)
            h = torch.cat( (h, e), dim=-1) # (1, B, C+H)
            # forward one step
            h, hx = self.lstm(h, hx) # (1, B, H), ((L, B, H), (L, B, H))
            # project
            p = F.linear(h, self.embeddings) # (1, B, K)
            p_list.append(p)
            if y is None: # use argmax
                yt = torch.argmax(p, dim=-1) # (1, B)
            else: # use answer
                yt = y[None, :, t] # (1, B)
            e = F.embedding(yt, self.embeddings) # (1, B, H)
            
        p = torch.cat(p_list, dim=0).transpose(0, 1)
        return p, hx
        """
        h, hx = self.lstm(x, hx)
        p = F.linear(h, self.embeddings)
        return p, hx



class CIF(nn.Module):
    def forward(self, e, a, max_l, nf, nl=None):
        # e: (B, T, C); features
        # a: (B, T); measure
        # max_l: int; max number of token
        # nf: (B)
        # nl: (B) 
        
        def create_attn_mask(a, max_l):
            s = a.cumsum(dim=-1).unsqueeze(1) # (B, 1, T)
            u = torch.arange(0, max_l, device=a.device).view(1, -1, 1)
            r0 = F.hardtanh(s - u, 0.0, 1.0)
            zeros = torch.zeros(1, 1, 1, device=a.device)
            r1 = torch.diff(r0, dim=2, prepend=zeros.expand(a.shape[0], max_l, 1))
            return r1
        
        if nl is not None:
            # 知道總共的 token 數
            a = a * (nl / a.sum(dim=-1)).view(-1, 1) # rescale the length
            
        mask = create_attn_mask(a, max_l) # (B, L, T)
        
        c = mask.matmul(e)
        return c, mask, a


class ASR0(nn.Module):
    def __init__(self, in_channels=80, num_classes=3000, h_channels=512, kernel_size=10, num_layers=8):
        super().__init__()
        # encoder
        self.lstm0 = nn.LSTM(in_channels, h_channels//2, num_layers, dropout=0.0, bidirectional=True, batch_first=True)
        # CIF
        self.conv0 = nn.Conv1d(h_channels, h_channels, kernel_size, padding=(kernel_size - 1)//2) 
        self.conv1 = nn.Conv1d(h_channels, 1, 1) # calculate alpha
        self.cif = CIF()
        # decoder_asr
        self.lstmp = LSTMP(h_channels, num_classes, h_channels, num_layers=2)
        
    def forward(self, x, y):
        # x: (Size(B,T,C), Size(B))
        # y: (Size(B,L), Size(B))
        x, x_l = x[0], x[1]
        y, y_l = y[0], y[1]
        
        # encode
        h, _ = self.lstm0(x)
        
        # cif
        c = h.transpose(1, 2)
        c = self.conv0(c)
        c = F.silu(c)
        a = self.conv1(c)
        a = F.sigmoid(a).squeeze(1) # (B, T)
        
        max_l = y.shape[1]
        c, attn_mask, a_ = self.cif(h, a, max_l, x_l, y_l)
        
        # decode
        p, _ = self.lstmp(c, y=y) # (B, L, K)
        p = F.log_softmax(p, dim=-1).transpose(1, 2) # (B, K, L)
        
        # calculate the log_prob
        nll_loss = F.nll_loss(p, y, ignore_index=0)
        
        # quantity loss 
        q_loss = F.l1_loss(a.sum(dim=-1), y_l.to(a.dtype))

        return nll_loss, q_loss

    def forward_raw(self, x):
        x, x_l = x[0], x[1]
        # encode 
        h, _ = self.lstm0(x)
        # cif
        c = h.transpose(1, 2)
        c = self.conv0(c)
        c = F.silu(c)
        a = self.conv1(c)
        a = F.sigmoid(a).squeeze(1) # (B, T)

        y_l = torch.ceil(a.sum(dim=1))
        max_l = int(y_l.max().item())
        c, attn_mask, a_ = self.cif(h, a, max_l, x_l, None)

        # decode 
        p, _ = self.lstmp(c) # (B, L, K)
        p = F.log_softmax(p, dim=-1).transpose(1, 2) # (B, K, L)
        return p, a_, attn_mask